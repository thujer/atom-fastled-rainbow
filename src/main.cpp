// RGB LED modul WS2811

// https://randomnerdtutorials.com/esp8266-pinout-reference-gpios/

#include <Arduino.h>
#include <Adafruit_NeoPixel.h>

#define PIN 4

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 16

int delayval = 100; // delay for half a second

Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

void setup() {

    Serial.begin(115200);

    delay(1000);

    pixels.begin();
    pixels.clear();
    pixels.setBrightness(50);
}

void loop() {

    for(int i=0; i<NUMPIXELS; i++) {
        pixels.setPixelColor(i, pixels.Color(0, 150, 0));
        pixels.show();
        delay(delayval);
    }

    for(int i=0; i<NUMPIXELS; i++) {
        pixels.setPixelColor(i, pixels.Color(150, 0, 0));
        pixels.show();
        delay(delayval);
    }

    for(int i=0; i<NUMPIXELS; i++) {
        pixels.setPixelColor(i, pixels.Color(0, 0, 150));
        pixels.show();
        delay(delayval);
    }

    for(int i=0; i<NUMPIXELS; i++) {
        pixels.setPixelColor(i, pixels.Color(150, 150, 0));
        pixels.show();
        delay(delayval);
    }

    for(int i=0; i<NUMPIXELS; i++) {
        pixels.setPixelColor(i, pixels.Color(150, 0, 150));
        pixels.show();
        delay(delayval);
    }

    for(int i=0; i<NUMPIXELS; i++) {
        pixels.setPixelColor(i, pixels.Color(150, 150, 150));
        pixels.show();
        delay(delayval);
    }

    for(int i=0; i<NUMPIXELS; i++) {
        pixels.setPixelColor(i, pixels.Color(0, 0, 0));
        pixels.show();
        delay(delayval);
    }

}